# FLISoL Chile 2020


El Festival Latinoamericano de Instalación de Software Libre FLISoL es el evento de difusión de software y cultura libres más grande de Latinoamérica. Este Festival, que comenzó en el 2005, ha tenido una gran acogida en la región. Es un evento distribuido que une los esfuerzos de voluntarios en mas de 20 países de la región y el mundo con el fin de dar a conocer a los asistentes, en más de 200 ciudades, la importancia del trabajo en comunidad y del software y la cultura libre.

Este 2020 se celebra el sábado 25 de abril

En este festival se pueden encontrar distintas actividades entre las cuales se destacan:

Instalación de Software Libre: Las personas que deseen obtener software libre pueden llevar sus computadores y un equipo de la comunidad se encargará de instalarlo. Si la persona lo desea también puede aprender el proceso de instalación para su propio beneficio.

Muestra de cultura libre: La cultura libre es la expresión artística de las nuevas tendencias culturales. Poesía, Poesía Visual, Poesía Experimental, Video arte, Cine arte, Arte digital, Net-art, Arte correo, Música, Arte de acción y de Performance, Literatura, Fotografía y en general toda expresión artística que permita el compartir.

Charlas, Talleres y Demostraciones de proyectos y de software libre: Personas expertas de la comunidad te presentarán de manera clara el software libre que puedes utilizar en tus actividades cotidianas, así como los proyectos en los cuales la comunidad trabaja.

El evento es GRATUITO y está dirigido a todo tipo de público: estudiantes, académicos, empresarios, trabajadores, funcionarios públicos, entusiastas e incluso personas que no poseen mucho conocimiento informático, pero que tiene deseos de conocer alternativas tecnológicas innovadoras y libres.


Organizador@s

Comunidad Nacional de Software Libre y Cultura Libre CNSL.CL

Coordinadoras de Chile


Lissa Gianini y Marcela Rosen


¿Desea contactar a l@s organizador@s en Chile?

Para inscribir una ciudad comunícate con la coordinación de tu país, para pedir información sobre el evento en el país, formas de vinculación y sugerencias, por favor escribe un correo a:

Grupo abierto de Telegram 'FLISoL Valparaíso'

https://t.me/FLISoL_Valpo

Si tu ciudad no aparece y ya te comunicaste con la coordinación de tu país (contacto@cnsl.cl), anímate a organizar la FLISoL en tu ciudad. Crea su página ingresando su nombre sin acentos ni espacios y pincha "Crear Ciudad". Aparecerá una plantilla wiki que puedes editar libremente y por favor, no borres la categoría al pie! Si necesitas ayuda escribe un correo electrónico a contacto@cnsl.cl o un mensaje de Telegram a @abuelitagnu

Si tu ciudad ya está, ¡Comunícate con los organizadores para darles tu apoyo, siempre faltan manos!

Como organizar FLISoL en una sede -ciudad- en Chile

Si quieres organizar una sede porque ya averiguaste que en tu ciudad no se hará FLISoL, ¡Eres Binvenid@! Lo ideal es que sean un grupo de personas que trabajen unidos para organizar una FLISoL porque es bastante trabajo, genera una comunidad! Comunícate con la Coordinación Nacional a contacto@cnsl.cl, busca material descargable en: Recursos Para Sedes

Únete al grupo FLISoL Chile de Telegram y al grupo de Organizadores de sedes FLISoL Chile - Coordinación sedes para que te invitemos escríbele a @abuelitagnu

Descarga material gráfico

Acá encuentras afiches editables, chapita, logos, certificados, credenciales, etc.: https://flisol.info/FLISOL2019/Chile/MaterialGrafico

Webs de Chile

Mayor detalle en cnsl.cl y flisol.cl

¿Qué necesita para instalar Software Libre?

Para instalar un sistema operativo libre en su computador es importante tener en cuenta que es altamente recomendado realizar un respaldo de sus datos y/o softwares preinstalados antes de asistir a uno de estos festivales, pues existe un mínimo riesgo de que los mismos se pierdan, como en cualquier instalación y configuración de software o sistemas operativos. Respaldar los datos y archivos e
n un disco duro externo o en CDs o pendrives ("nunca está demás hacer un respaldo cada cierto tiempo"). Y si desean seguir con Window$ en su equipo hay que hacer un análisis y una desfragmentación del disco duro un día antes de venir a FLISoL (la desfragmentación y el análisis son tareas de mantención habituales de Window$, se pueden encontrar en el Panel de control o en Accesorios -> Herramientas del Sistema).

Auspicios

Este espacio espera a las empresas chilenas que quieran aportar a la causa del software libre!